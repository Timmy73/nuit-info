# Installer la database
## Tables
```bash
  sqlite3 users.db
  sqlite-> .read user_create.sql
  sqlite-> .read thread_create.sql
```

## Jeu de Test
```bash
  sqlite3 users.db
  sqlite-> .read test_user_insert.sql
  sqlite-> .read test_thread
```
