PRAGMA foreign_keys = ON;

Drop TABLE Thread;
DROP TABLE User;
DROP TABLE University;
DROP TABLE Rank;
DROP TABLE Response;
DROP TABLE Category;
DROP TABLE Documentation;
DROP TABLE Keyword;
DROP TABLE Highscore;

CREATE TABLE User (
  id integer PRIMARY KEY AUTOINCREMENT,
  login varchar(255) UNIQUE NOT NULL,
  hashpass varchar(255) NOT NULL,
  xp integer NOT NULL,
  univ varchar(255),
  email varchar(255),
  FOREIGN KEY (univ) REFERENCES University(nameU)
);

CREATE TABLE University (
    nameU varchar(255) PRIMARY KEY,
    region varchar(255)
);


CREATE TABLE Rank (
  id integer PRIMARY KEY AUTOINCREMENT,
  name varchar(255) UNIQUE NOT NULL,
  xpmin integer NOT NULL,
  xpmax integer
);

CREATE TABLE Thread (
  id integer PRIMARY KEY AUTOINCREMENT,
  title VARCHAR(255),
  author INTEGER,
  cat VARCHAR(255),
  FOREIGN KEY (author) REFERENCES User(id)
  FOREIGN KEY (cat) REFERENCES Category(nameC)
);

CREATE TABLE Response (
  idR INTEGER PRIMARY KEY AUTOINCREMENT,
  idT INTEGER,
  author INTEGER,
  content TEXT NOT NULL,
  upvotes INTEGER DEFAULT 0,
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (idT) REFERENCES Thread(id) ON DELETE CASCADE,
  FOREIGN KEY (author) REFERENCES User(id)
);

CREATE TABLE Category (
    nameC varchar(255) PRIMARY KEY
);

CREATE TABLE Keyword (
    id varchar(255) PRIMARY KEY
);

CREATE TABLE Documentation (
    urlpath varchar(255) PRIMARY KEY,
    title varchar(255) 
);

CREATE TABLE Highscore (
  pseudo varchar(255) NOT NULL,
  score integer NOT NULL
);

INSERT INTO University VALUES
("UGA","Auvergne-Rhône-Alpes"),
("Lyon 1","Auvergne-Rhône-Alpes"),
("Université de Valence","Auvergne-Rhône-Alpes"),
("Université Paris Sud","Ile de France")
;

INSERT INTO User VALUES
(109, "Martin","zdve3fezf",0,"UGA","martin@gmail.com"),
(2, "Oui","zddzadave3fezf",5,null,"oui.hotmail.fr"),
(3, "Leo","zdve3fddfezf",409,"UGA","oel@gmail.com"),
(4, "Franck","zdve3f564ezf",493,"Lyon 1","xxfranckxx@gmail.com"),
(5, "Joestar","zdve3555fezf",103,"Université Paris Sud","jojo@laposte.fr"),
(6, "Patrick","zdve3fe876zf",403423593,"Université de Valence","patou@free.fr"),
(7,'Martindu69',"fffffff",1,null,"lolmdr@laposte.fr")
;


INSERT INTO Rank(name,xpmin,xpmax) VALUES
("Débutant",0,100),
("Membre",100,500),
("Expert",500,9223372036854775807); -- Ugly but hey, it works

INSERT INTO Category VALUES
("Actualités"),
("Aide"),
("aefijefi")
;

INSERT INTO Thread(title,author, cat) VALUES
("Aidez moi",2, "Actualités"),
("Assurance ?",3, "Aide"),
("Oui",109, "Aide"),
("zaepofkjaezpokogpeajgpezogpzngzpgnezognez",7, "Aide")
;


INSERT INTO Response(idT,author, content, upvotes) VALUES
(1,2,"cA MARCHE PAS AIDEZ MOI",45),
(2,3,":)",0),
(2,109,"ok",2),
(4,3,"AZLIFNAZOINFAZIOFNIOZAFmzfamzofnazmofnzofmnzefmonefomezngmlzenglezmgnezmlgnzelmgnezlgmnezlgeznvklzenvlkzmengezmlknbezklmngzemlkng8",1)
;

INSERT INTO Keyword VALUES
("probleme"),
("lol"),
("Tours")
;

INSERT INTO Documentation VALUES
("truc.fr/machin/doc/assuarance.htm","guide assurance"),
("truc.fr/machin/doc/administration.html","dossier a rendre")
;

INSERT INTO Highscore VALUES
("patrick",2324),
("Tom",2)
;