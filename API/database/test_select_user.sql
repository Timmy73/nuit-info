SELECT * FROM User;
SELECT * FROM Rank;

SELECT R.name
FROM Rank R, User U
WHERE  R.xp < U.xp AND U.login == 'Martin'
ORDER BY R.xp DESC
LIMIT 1;

SELECT R.name
FROM Rank R, User U
WHERE  R.xp < U.xp AND U.login == 'Leo'
ORDER BY R.xp DESC
LIMIT 1;

SELECT U1.login, U2.login
FROM User U1, User U2
WHERE U1.univ == U2.univ AND U1.id > U2.id;
