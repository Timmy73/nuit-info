const express = require('express')
const bodyParser = require("body-parser");
const cors = require('cors');
const app = express()
const port = 3000

let user = require('./routes/user.js');
let thread = require('./routes/thread.js');
let response = require('./routes/response.js');
let university = require('./routes/university');
let chatbot = require('./routes/chatbot.js');
let categories = require('./routes/category');
let highscore = require('./routes/highscore.js');

let KeywordThread = require('./models/keywordthread');
KeywordThread.create({idT: 1, idK: "lol"}).then(a => {
    KeywordThread.create({idT: 2, idK: "Tours"}).then(a => {
        KeywordThread.create({idT: 2, idK: "probleme"}).then(a => {
            
        })
    })
})

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/user',user);
app.use('/thread',thread);
app.use('/response',response);
app.use('/university',university);
app.use('/chatbot',chatbot);
app.use('/category', categories);
app.use('/highscore',highscore);

app.get('/', (req, res) => res.send('Hello World!'))


app.listen(port, () => console.log(`Example app listening on port ${port}!`))
