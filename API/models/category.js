const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const Model = Sequelize.Model;

const Thread = require('./thread');

class Category extends Model{}

Category.init({
    nameC:{
        primaryKey: true,
        type: Sequelize.INTEGER,    
    }
}, {
    sequelize,
    modelName: 'category',
    tableName: 'Category',
    timestamps: false,
});

Category.hasMany(Thread, {foreignKey: "cat", sourceKey: "nameC"});
Thread.belongsTo(Category, {foreignKey: "cat", sourceKey: "nameC"});

Category.sync({force: false}).then(() => {

});

module.exports = Category;