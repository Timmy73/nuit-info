const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const Model = Sequelize.Model;

class Documentation extends Model{}

Documentation.init({
    urlpath:{
        primaryKey: true,
        type: Sequelize.STRING
    },
    title:{
        type: Sequelize.STRING
    }
},{
    sequelize,
    modelName: 'documentation',
    tableName: 'Documentation',
    timestamps: false
});

Documentation.sync({force: false}).then(() => {

});

module.exports = Documentation;