const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const Model = Sequelize.Model;

class Highscore extends Model{}

Highscore.init({
    pseudo:{
        type: Sequelize.STRING
    },
    score:{
        type: Sequelize.INTEGER
    }
},{
    sequelize,
    modelName: 'highscore',
    tableName: 'Highscore',
    timestamps: false
});

Highscore.sync({force: false}).then(() => {

});

module.exports = Highscore;