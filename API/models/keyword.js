const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const Model = Sequelize.Model;

const Thread = require('./thread');
const Documentation = require('./documentation');
const KeywordThread = require('./keywordthread');

class Keyword extends Model {}

Keyword.init({
    id:{
        primaryKey: true,
        type: Sequelize.STRING
    }
}, {
    sequelize,
    modelName: 'keyword',
    tableName: 'Keyword',
    timestamps: false
});

Thread.belongsToMany(Keyword, {through: "KeywordThread", foreignKey: "idT", otherKey: "idK"});
Documentation.belongsToMany(Keyword, {through: "KeywordDocumentation", foreignKey: "idD", otherKey: "idK"})

Keyword.sync({force: false}).then(() => {

});

sequelize.sync({force: false});

module.exports = Keyword;