const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const Model = Sequelize.Model;

class KeywordThread extends Model {}

KeywordThread.init({
    idT:{
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    idK:{
        primaryKey: true,
        type: Sequelize.STRING
    }
}, {
    sequelize,
    modelName: 'keywordthread',
    tableName: 'KeywordThread',
});

KeywordThread.sync({force: false}).then(() => {

});

module.exports = KeywordThread;