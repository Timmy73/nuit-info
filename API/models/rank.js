const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const Model = Sequelize.Model;

class Rank extends Model {}

Rank.init({
    id: {
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    xpmin: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    xpmax: {
        type: Sequelize.INTEGER
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    sequelize,
    modelName: 'rank',
    tableName: 'Rank',
    timestamps: false
});

Rank.sync({force: false}).then(() => {

});

module.exports = Rank;