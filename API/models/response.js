const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const Model = Sequelize.Model;

const User = require('./user');

class Response extends Model {}
Response.init({
  idR: {
      primaryKey: true,
      type: Sequelize.INTEGER,
      autoIncrement: true
  },
  idT: {
      type: Sequelize.INTEGER,
  },
  author: {
      type: Sequelize.INTEGER,
  },
  content: {
      type: Sequelize.TEXT,
      allowNull: false
  },
  upvotes: {
      type: Sequelize.INTEGER,
      defaultValue: 0
  },
  timestamp: {
      type: Sequelize.DATE
  }
}, {
  sequelize,
  modelName: 'response',
  tableName: 'Response',
  timestamps: false
});

User.hasMany(Response, {foreignKey: "author", sourceKey: "id"});
Response.belongsTo(User, {foreignKey: "author", sourceKey: "id"});

Response.sync({ force: false }).then(() => {

});

module.exports = Response