const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const Model = Sequelize.Model;

const Response = require('./response');
const User = require('./user');

class Thread extends Model {}

Thread.init({
    id:{
        primaryKey:true,
        type: Sequelize.INTEGER
    },
    title:{
        type: Sequelize.STRING
    },
    author:{
        type: Sequelize.INTEGER
    },
    cat: {
        type: Sequelize.STRING
    }
},{
    sequelize,          
    modelName: 'thread',
    tableName: 'Thread',
    timestamps: false
  });

Thread.hasMany(Response, {foreignKey: "idT", sourceKey: "id"});
Response.belongsTo(Thread, {foreignKey: "idT", sourceKey: "id"});

User.hasMany(Thread, {foreignKey: "author", sourceKey: "id"});
Thread.belongsTo(User, {foreignKey: "author", sourceKey: "id"});

Thread.sync({ force: false }).then(() => {

});


module.exports = Thread