const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const Model = Sequelize.Model;

const User = require('./user');

class University extends Model {}

University.init({
    nameU: {
        primaryKey: true,
        type: Sequelize.STRING
    }
}, {
    sequelize,
    modelName: 'university',
    tableName: 'University',
    timestamps: false
});

University.hasMany(User, {foreignKey: "univ", sourceKey: "nameU"});
User.belongsTo(University, {foreignKey: "univ", sourceKey: "nameU"});

University.sync({force: false}).then(() => {

});

module.exports = University;