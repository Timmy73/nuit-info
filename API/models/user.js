const Sequelize = require('sequelize');
const sequelize = require('../sequelize');

const Model = Sequelize.Model;
const Op = Sequelize.Op;

const Rank = require("./rank");

class User extends Model {}
User.init({
  id: {
      primaryKey: true,
      type: Sequelize.INTEGER,
  },
  login: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
  },
  hashpass: {
      type: Sequelize.STRING,
      allowNull: false,
  },
  xp: {
      type: Sequelize.INTEGER,
      allowNull: false
  },
  univ: {
      type: Sequelize.STRING,
  }
}, {
  sequelize,
  modelName: 'user',
  tableName: 'User',
  timestamps: false
});

User.prototype.getRank = function() {
    return Rank.findOne({where: {
        xpmax: {
            [Op.gt]: this.xp
        },
        xpmin: {
            [Op.lte]: this.xp
        }
    }})
}

User.sync({ force: false }).then(() => {

});

module.exports = User