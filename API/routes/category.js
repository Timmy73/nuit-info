const Category = require('../models/category');
const express = require('express');

const Thread = require('../models/thread');

const router = express.Router();

router.get("/", (req, res) => {
    Category.findAll({include: [Thread]}).then(categories => {
        res.json(categories);
    })
})

router.get("/:id", (req, res) => {
    Category.findByPk(req.params.id, {include: [Thread]}).then(category => {
        if (category == null) {
            res.status(404).json("Category " + req.params.id + " not found.")
        } else {
            res.json(category)
        }
    })
})

module.exports = router;