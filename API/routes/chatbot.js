const Keyword = require('../models/keyword');
const KeywordThread = require('../models/keywordthread');
const express = require('express');

const router = express.Router();

function getCount(keywords, kw) { // Get the occurences number
  var res = keywords.filter(e => e[0] == kw);
  if(res.length > 0){
    res = res[0];
    return res[1];
  }
  else
    return 0;
}

function getPertinance(keywords, kwList) { // Compute the thread weight
  var sum = 0;
  for (var i = 0; i < kwList.length; i++)
    sum += getCount(keywords, kwList[i])
  return sum;
}

router.post("/request", (req, res) => {
    var text = req.body.text;
    Keyword.findAll().then((keywords) => { // Get all keywords
      keywords = keywords.map(e => e.id); // Tranform into a simple string array

      text = text.split(" ").filter( // Split text given in body request
        e => keywords.indexOf(e) != -1
      );

      var kw = keywords.map( // Count keywords occurences 
        e => [e, text.filter(
          w => w == e
        ).length]
      );

      kw.sort( // Sort the array (desc)
        (w1, w2) => w2[1] - w1[1]
      );

      KeywordThread.findAll().then((tags) => { // We group keywords used by each thread
        var threads = {};
        tags.map(t => {
          (threads[t.idT] = threads[t.idT] || []).push(t.idK);
        });

        var tValue = [];

        Object.keys(threads).map((e, i) => { // Evaluate for each thread is weight
          tValue[i] = [e, getPertinance(kw, threads[e])];
        });

        tValue.sort( // We sort threads by weight (desc)
          (w1, w2) => w2[1] - w1[1]
        );

        res.json(tValue.slice(0,5).map(e => e[0])); // We return top 5 of thread
      });
    });
});

module.exports = router;
