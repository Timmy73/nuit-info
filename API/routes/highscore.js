const Highscore = require('../models/highscore');
const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
    Highscore.findAll().then(highscore => {
        res.json(highscore);
    });
})

module.exports = router