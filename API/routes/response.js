const Response = require('../models/response');
const express = require('express');

const router = express.Router();

const User = require('../models/user');

router.get("/:id", (req, res) => {
    Response.findByPk(req.params.id, {include: [User]}).then(response => {
        if (response == null) {
            res.status(404).json("Response " + req.body.id + " not found.");
        } else {
            res.json(response);
        }
    })
})

router.post("/:id", (req, res) => {
    Response.findByPk(req.params.id).then(response => {
        if (response == null) {
            res.status(404).json("Response " + req.body.id + " not found.");
        } else {
            response.set("upvotes", response.upvotes + 1);
            response.save().then(() => {
                res.status(204).json();
            })
        }
    })
})

module.exports = router