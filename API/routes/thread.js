const Thread = require('../models/thread');
const Response = require('../models/response');
const express = require('express');

const router = express.Router();

const User = require('../models/user');
const Keyword = require('../models/keyword')

router.get('/', (req, res) => {
    Thread.findAll({include: [User, Keyword]}).then(threads => {
        res.json(threads);
    })
})

router.get('/:id', (req, res) => {
    Thread.findByPk(req.params.id, {include: [User, Keyword]}).then(thread => {
        if (thread == null) {
            res.status(404).json("Thread " + req.params.id + " not found.")
        } else {
            res.json(thread);
        }
    }).catch(err => {
        res.status(500).json(err);
    });
});

router.post("/", (req,res) => {
    Thread.create(req.body).then(thread => {
        res.status(201).json({});
    }).catch(err => {
        res.status(500).json(err.errors[0].message);
    });
});

router.post("/:id", (req, res) => {
    Thread.findByPk(req.params.id).then(thread => {
        if (thread == null) {
            res.status(404).json("Thread " + req.params.id + " not found.");
        } else {
            Response.create(req.body, {include: [User]}).then(response => {
                response.setThread(thread).then(() => {
                    res.status(201).json(response);
                });
            })
        }
    });
});

router.get("/:id/response", (req, res) => {
    Thread.findOne({
        where: {id: req.params.id}, 
        include: [{model:Response, include: [User]}],
        order: [[Response, 'timestamp', 'asc']] 
        }).then(thread => {
            if (thread == null) {
                res.status(404).json("Thread " + req.params.id + " not found.");
            } else {    
                res.status(200).json(thread.responses);
            }
    })
})

router.delete("/:id", (req, res) => {
    Thread.findByPk(req.params.id).then(thread => {
        if (thread == null) {
            res.status(404).json("Thread " + req.params.id + " not found.")
        } else {
            thread.destroy().then(() => {
                res.status(204).json();
            })
        }
    }).catch(err => {
        res.status(500).json(err.errors[0].message);
    }) ;
})

module.exports = router