const University = require('../models/university');
const express = require('express');

const router = express.Router();

router.get("/", (req, res) => {
    University.findAll().then(unis => {
        res.status(200).json(unis);
    });
})

module.exports = router