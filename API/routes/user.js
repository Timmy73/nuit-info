const User = require('../models/user');
const express = require('express');

const router = express.Router();

const University = require('../models/university');

router.get('/', (req, res) => {
    User.findAll({include: [University]}).then((users) => {
        res.json(users);
    });
});

router.get('/:id', (req, res) => {
    User.findByPk(req.params.id, {include: [University]}).then(user => {
        if (user == null) {
            res.status(404).json("User " + req.params.id + " not found.")
        } else {
            res.json(user);
        }
    }).catch(err => {
        res.status(500).json(err);
    });
});

router.get('/:id/rank', (req, res) => {
    User.findByPk(req.params.id).then(user => {
        if (user == null) {
            res.status(404).json("User " + req.params.id + " not found.")
        } else {
            user.getRank().then(rank => {
                res.json(rank);
            })
        }
    }).catch(err => {
        res.status(500).json(err);
    });
})

router.post('/', (req, res) => {
    User.create(req.body).then(user => {
        res.status(201).json({});
    }).catch(err => {
        res.status(403).json(err.errors[0].message);
    });
});

router.put('/:id',(req,res) => {
    User.update(req.body, {where: {id: req.params.id}}).then(user => {
        res.status(204).json();
    }).catch(err => {
        res.status(403).json(err.errors[0].message);
    });
});

router.delete('/:id', (req, res) => {
    User.findByPk(req.params.id).then(user => {
        if (user == null) {
            res.status(404).json("User " + req.params.id + " not found.")
        } else {
            user.destroy().then(() => {
                res.status(204).json();
            })
        }
    }).catch(err => {
        res.status(500).json(err.errors[0].message);
    }) ;
});

module.exports = router