import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClarityModule } from '@clr/angular';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { NewsfeedComponent } from './newsfeed/newsfeed.component';
import { Game404Component } from './game404/game404.component';
import { FormsModule } from '@angular/forms';
import { IsSignedInGuard } from './guard/sessionguard';
import { Session } from './model/session';
import { ForumComponent } from './forum/forum.component';
import { ThreadComponent } from './thread/thread.component';
import { ChatbotComponent } from './chatbot/chatbot.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'news', component: NewsfeedComponent},//, canActivate: [IsSignedInGuard] },
  { path: '404', component: Game404Component},
  { path: 'signup', component: SignupComponent },
  { path: 'forum', component: ForumComponent , canActivate: [IsSignedInGuard] },
  { path: 'thread/:id', component: ThreadComponent },
  { path: 'chatbot', component: ChatbotComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: '404'}

];

@NgModule({
  declarations: [AppComponent, HomeComponent, LoginComponent, NewsfeedComponent, HeaderComponent, FooterComponent, Game404Component, SignupComponent,ForumComponent,ChatbotComponent, ThreadComponent],
  imports: [
    HttpClientModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
    BrowserModule,
    BrowserAnimationsModule,
    ClarityModule,
    FormsModule
  ],
  providers: [Session],
  bootstrap: [AppComponent]
})
export class AppModule {}
