import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Thread } from '../model/thread';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.css']
})
export class ChatbotComponent implements OnInit {
  answers: Thread[];
  query: string;
  isDisplayed:boolean;
  constructor(private readonly http: HttpClient) {
    this.query = '';
    this.answers = [];
    this.isDisplayed = false;
  }

  ngOnInit() {
  }
  private async send() {
    this.isDisplayed = false;
    let ids: string[] = [];
    this.answers = [];
    console.log(this.query);
    await this.http.post<string[]>('http://localhost:3000/chatbot/request', {text: this.query}).subscribe(res => {res.forEach(element => {
      this.http.get<Thread>('http://localhost:3000/thread/' + element).toPromise().then((value) => this.answers.push(value));
    }); this.isDisplayed = true;});
  }
}
