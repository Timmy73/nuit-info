import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Thread, ThreadResults } from '../model/thread';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css']
})
export class ForumComponent implements OnInit {
  threads: Thread[];

  constructor(private readonly http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.getThreads();
  }

  private getThreads() {
    this.http.get<Thread[]>(`http://localhost:3000/thread`).toPromise().then(threads => {
      console.log(threads);
      this.threads = threads;
    });
  }
}
