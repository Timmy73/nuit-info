import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Game404Component } from './game404.component';

describe('Game404Component', () => {
  let component: Game404Component;
  let fixture: ComponentFixture<Game404Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Game404Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Game404Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
