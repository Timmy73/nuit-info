import { Component, OnInit } from '@angular/core';
declare var loadGame: any;

@Component({
  selector: 'app-game404',
  templateUrl: './game404.component.html',
  styleUrls: ['./game404.component.css']
})
export class Game404Component implements OnInit {

  constructor() { }

  ngOnInit() {
    loadGame();
  }

}