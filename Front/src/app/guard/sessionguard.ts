import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Session } from '../model/session';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class IsSignedInGuard implements CanActivate {

    constructor(private router: Router,
                private session: Session) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.session.isSignedIn()) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }

}