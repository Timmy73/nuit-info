import { Component, OnInit } from '@angular/core';
import { Session } from '../model/session';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public readonly session: Session) { }

  ngOnInit() {
  }

  isConnected() {
    return this.session.isConnected;
  }

}
