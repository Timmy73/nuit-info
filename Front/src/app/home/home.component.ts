import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Content, ContentResponse } from '../model/content';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  contents: Content[];
  constructor() {
  }

  ngOnInit() {}

}
