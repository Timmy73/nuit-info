import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, UserResults } from '../model/user';
import { Session } from '../model/session';
import { Router } from '@angular/router';
import { element } from 'protractor';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  mdp: string;
  hasError: string;
  constructor(private readonly http: HttpClient,
              private session: Session,
              private router: Router) {
    this.hasError = 'error';
  }

  ngOnInit() {}

  private async login() {
    let found: User;
    this.hasError = 'error';
    const results = await this.http
      .get<User[]>(`http://localhost:3000/user`)
      .toPromise()
      .then(value => {
        console.log(value);
        value.forEach(element => {
          if (
            element.login === this.username &&
            element.hashpass === this.mdp
          ) {
            found = element;
          }
        });
        if (!found) {
          this.hasError += ' active';
        } else {
          this.session.isConnected = true;
          this.session.username = this.username;
          this.session.id = found.id;
          this.router.navigate(['/home']);
        }

      });
  }
}
