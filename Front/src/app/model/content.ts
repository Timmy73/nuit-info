export interface Content {
    title: string;
    content: string;
}

export interface ContentResponse {
    results: Content[];
}

