export interface Response {
    idR: number;
    idT: number;
    content: string;
    author: string;
    upvotes: number;
    timestamp: Date;
}