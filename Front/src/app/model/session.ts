import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'  
})

export class Session {
    username: string;
    isConnected: boolean;
    id: number;

    isSignedIn() {
        return this.isConnected;
    }
}