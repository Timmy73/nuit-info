export interface Thread {
    id: number;
    title: string;
    author: number;
    category: string;
}

export interface ThreadResults {
    results: [];
}
