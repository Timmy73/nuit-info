export interface User {
    id: number;
    login: string;
    mail: string;
    hashpass: string;
    xp: number;
    univ: string;
}

export interface UserResults {
    results: User[];
}