import { Component, OnInit } from '@angular/core';
import { HttpClient  } from "@angular/common/http";
import * as xml2js from "xml2js";
import { NewsRss } from './news-rss';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-newsfeed',
  templateUrl: './newsfeed.component.html',
  styleUrls: ['./newsfeed.component.css']
})

export class NewsfeedComponent implements OnInit {
  RssData: NewsRss;
  url = "https://www.bfmtv.com/rss/info/flux-rss/flux-toutes-les-actualites/";
  constructor(private http: HttpClient,
    private sanitizer:DomSanitizer) { }
  ngOnInit() {
    this.GetRssFeedData();
  }
  GetRssFeedData() {
    const requestOptions: Object = {
      observe: "body",
      responseType: "text"
    };
    this.http
      .get<any>(this.url, requestOptions)
      .subscribe(data => {
        let parseString = xml2js.parseString;
        parseString(data, (err, result: NewsRss) => {
          result.rss.channel.forEach(c=>(console.log(c.image[0].url[0])));
          this.RssData = result;
        });
      });
  }
  ChangeUrl(s){
    this.url = "https://www.bfmtv.com/rss"+s;
    this.GetRssFeedData();
  }

  sanitize(url:string){
    return this.sanitizer.bypassSecurityTrustUrl(url);
}
}



  //"http://www.jeunes.gouv.fr/spip.php?page=rss_thematiques_interministerielles"



