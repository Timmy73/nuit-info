import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Session } from "../model/session";
import { Router } from "@angular/router";
import { User } from "../model/user";
import { Universite } from "../model/univ";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"]
})
export class SignupComponent implements OnInit {
  universites: Universite[];
  username: string;
  mdp: string;
  mdpconf: string;
  mail: string;
  uni: string;
  mismatch: string;
  alreadyExist: string;
  success: string;
  constructor(
    private readonly http: HttpClient,
    private router: Router
  ) {
    this.mismatch = "error";
    this.alreadyExist = "error";
    this.success = "error";
    this.getUniv().then((value) => this.universites = value);
  }

  ngOnInit() {}

  private async getUniv() {
    return await this.http.get<Universite[]>(`http://localhost:3000/university`).toPromise();
  }

  private compMdp(): boolean {
    if (this.mdp !== this.mdpconf) {
      this.mismatch += " active";
      return false;
    } else {
      this.mismatch = "error";
      return true;
    }
  }

  private register() {
    this.success = "error";
    if (this.compMdp()) {
      let found: boolean = false;
      const results = this.http
        .get<User[]>(`http://localhost:3000/user`)
        .toPromise()
        .then(value => {
          console.log(value);
          value.forEach(element => {
            if (
              element.login === this.username
            ) {
              found = true;
            }
          });
          if (found) {
            this.alreadyExist += " active";
          } else {
            this.alreadyExist = "error";
            const user = {
              login: this.username,
              hashpass: this.mdp,
              xp: 0,
              univ: this.uni
            };
            console.log(user);
            this.http.post('http://localhost:3000/user', user).subscribe(err => console.log(err));
            this.success = "alert alert-success";
          }
        });
    }
  }
}
