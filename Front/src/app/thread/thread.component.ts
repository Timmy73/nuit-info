import { Component, OnInit } from '@angular/core';
import { Thread } from '../model/thread';
import { HttpClient } from '@angular/common/http';
import { Response } from '../model/response';
import { Session } from '../model/session';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-thread',
  templateUrl: './thread.component.html',
  styleUrls: ['./thread.component.css']
})
export class ThreadComponent implements OnInit {

  private id: number;
  private sub: any;
  private thread: Thread;
  private responses: Response[];
  public response: string;

  constructor(private route: ActivatedRoute, private readonly http: HttpClient, public readonly session: Session) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params.id;
      this.http.get<Thread>('http://localhost:3000/thread/' + this.id).toPromise().then(thread => {
        this.thread = thread;
      });
      this.http.get<Response[]>('http://localhost:3000/thread/' + this.id + '/response').toPromise().then(responses => {
        this.responses = responses;
      });
    });
  }

  like(responseId) {
    this.http.post('http://localhost:3000/response/' + responseId, {}).toPromise().then(_ => {
      this.responses.find(e => e.idR === responseId).upvotes += 1;
    });
  }

  post() {
    this.http.post<any>('http://localhost:3000/thread/' + this.id, {content: this.response, author: this.session.id}).toPromise().then(r => {
      r.user = { login: this.session.username };
      this.responses.push(r);
    });
  }
}
