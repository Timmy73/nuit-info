/*
 Fait par Thomas HERVE
 Dessins par Amine FARHAT

 svp jugez pas le code dur dur de passer de Unity à pixi.js
*/

class ObstacleG {
    constructor(obstacle,r){
        this.obstacle = obstacle;
        this.r = r;
    }
}
let hauteurPage; 
let app; 
function loadGame(){
    hauteurPage = window.innerHeight - document.getElementById("header").offsetHeight;
    let type = "WebGL"
    if(!PIXI.utils.isWebGLSupported()){
      type = "canvas"
    }
    app = new PIXI.Application({
        width: window.innerWidth * 0.99,         // default: 800
        height: hauteurPage,        // default: 600
        antialias: true,    // default: false
        transparent: false, // default: false
        resolution: 1       // default: 1
      }
    );
    app.stage.updateLayersOrder = function () {
        app.stage.children.sort(function(a,b) {
            a.zIndex = a.zIndex || 0;
            b.zIndex = b.zIndex || 0;
            return a.zIndex - b.zIndex
        });
        };

    PIXI.utils.sayHello(type)
    loader = PIXI.loader,
    //importer des sprites
    loader
      .add([
        "assets/images/camion.png",
        "assets/images/camion2.png",
        "assets/images/camion3.png",
        "assets/img/road.png",
        "assets/img/papers.png",
        "assets/img/buildings.png"
      ])
      for(let i = 0; i <= 59; i++){
        n = i.toString();
        if(i < 10)n = "0"+n;
        loader.add("assets/img/char/char_000"+n+".png");
      }
      loader.load(setup);
}



let coeffHauteur = 3/15;
let delta = window.innerWidth * coeffHauteur /10;
let decalage = (window.innerWidth * coeffHauteur / 3);
let players = [];
let routes = [];
let obstacles = [];
let currentObstacles = [];
let fonds = [];
let coeff;
let xposition;
let yposition;
let positionCentral;
let road = 2;
let nextCoord;
let addition = 1;
let beginAnimation = false;
let vitesseDefilement = 10;
let vitesseDefilementOriginal = vitesseDefilement;
let points = 0;
let currentFirstRoute = 0;
let animationSpeed = 0.5;
let originalAnimationSpeed = animationSpeed;
let currentAnimation = 0;
let currentAnimationTimer = 0;
let animationPlayerOn = true;
let scoreMessage;
let updatePoints = 60;
let timerPoints = 0;
let updateVitesse = 120;
let timerVitesse = 0;
let debut = true;
let debutMessage;
//timer summon obstacles
let obstacleSpeed = 80;
let originalObstacleSpeed = obstacleSpeed;
let currentObstacleTimer = 150;

function setup() {
  //musique
  
  

  //fonction core 
  resources = PIXI.loader.resources,
Sprite = PIXI.Sprite;

document.getElementById("container").appendChild(app.view);

app.renderer.backgroundColor = 0x29ABE2;
app.renderer.autoResize = true;
//app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoResize = true;
app.renderer.resize(window.innerWidth, hauteurPage);
  for(let i = 0; i <= 59; i++){
    n = i.toString();
    if(i < 10)n = "0"+n;
    players.push(new Sprite(resources["assets/img/char/char_000"+n+".png"].texture));
  }
  routes.push(new Sprite(resources["assets/img/road.png"].texture));
  routes.push(new Sprite(resources["assets/img/road.png"].texture));
  routes.push(new Sprite(resources["assets/img/road.png"].texture));

  obstacles.push(resources["assets/img/papers.png"].texture);

  fonds.push(new Sprite(resources["assets/img/buildings.png"].texture))
  fonds.push(new Sprite(resources["assets/img/buildings.png"].texture))
  fonds.push(new Sprite(resources["assets/img/buildings.png"].texture))

  coeffFond = fonds[0].height/fonds[0].width
  pos = 0;
  w = window.innerWidth;
  routes.forEach(route => {
    route.width = w;
    route.height = w * coeffHauteur;
    route.y = hauteurPage - route.height;
    route.x = pos;
    pos += w;
    route.zIndex = 5;
    route.nom = "route";
    app.stage.addChild(route);
  })

  pos = 0;
  fonds.forEach(fond => {
      fond.zIndex = 0;
      fond.width = w;
      fond.height = w*coeffFond;
      fond.y = routes[0].y - w*coeffFond;
      fond.x = pos;
      pos += w;
      app.stage.addChild(fond);
  })

  coeff = players[0].width/players[0].height;
  //player init
  xposition = window.innerWidth/10;
  yposition = hauteurPage - decalage * 2 + delta;
  positionCentral = yposition;
  nextCoord = yposition;

  c = 0;
  players.forEach(player => {
    player.anchor.set(0.5,0.5);
    player.x = xposition;
    player.y = yposition;
    player.zOrder = 50;
    player.vx = 0;
    player.vy = 80;
    player.zIndex = 50;
    player.nom = "player";
    if(c > currentAnimation){
        player.visible = false;
    }
    c++;
  })

   //taille dynamique du joueur
   players.forEach(player => {
    player.height = routes[0].height/3 * 1.5;
    player.width = routes[0].height/3 * 1.5 * coeff;  
    })
  

  players.forEach(player => {
    app.stage.addChild(player);
  })


  //points
  scoreMessage = new PIXI.Text("");
  app.stage.addChild(scoreMessage);

  //Debut
  let style = new PIXI.TextStyle({
    fontFamily: "Futura",
    fontSize: w/20,
    fill: "black"
  });
  debutMessage = new PIXI.Text("Appuyez sur fleche droite",style)
  app.stage.addChild(debutMessage);
  debutMessage.anchor.x = 0.5;
  debutMessage.x = w/2

  //lancement de la boucle de gameplay
  app.ticker.add(delta => gameLoop(delta));
}

//loop principale
function gameLoop(delta){
    if(debut){
        replay()
        space = keyboard("ArrowRight")
        space.press = () => {
            debutMessage.visible = false;
            debut = false;
        }
        return;
    }
    if(!animationPlayerOn){
        return;   
    }
    //taille dynamique de la route
    w = window.innerWidth;
    routes.forEach(route => {
        route.width = w;
        route.height = w * coeffHauteur;
        route.y = hauteurPage - route.height;
    })

    tp = false;
    p = 0
    routes.forEach(route => {
        if(route.x <= -w) {
            tp = true;
        }
        p++
    })

    if(!tp){
        compte = 0;
        routes.forEach(route => {
            route.x -= vitesseDefilement;
            fonds[compte].x -= vitesseDefilement;
            compte++;
        })
        //gestion des obstacles
        currentObstacles.forEach((obstacle)=>{
            obstacle.obstacle.x -= vitesseDefilement;
            if(obstacle.obstacle.x < -w) {
                removeObstacle(obstacle);
            }
            //detection de la mort
            if(obstacle.obstacle.x <= players[0].x + vitesseDefilement && obstacle.obstacle.x >= players[0].x - vitesseDefilement) {
                if(road == obstacle.r){
                    finDuJeu();
                }
            }
        })
        currentObstacleTimer += delta;
        if(currentObstacleTimer > obstacleSpeed){
            currentObstacleTimer = 0;
            addObstacles();
        }


    } else {
        for(let i = 0; i < routes.length; i++){
            if(routes[i].x <= -w){
                if(i == 0){
                    routes[i].x = routes[routes.length-1].x + w;
                    fonds[i].x = fonds[fonds.length-1].x + w;
                } else {
                    routes[i].x = routes[i-1].x + w;
                    fonds[i].x = fonds[i-1].x + w;
                }
            }
        }
    }

   

    //animaton du joueur
    currentAnimationTimer += delta
    if(currentAnimationTimer > animationSpeed && animationPlayerOn){
        currentAnimationTimer = 0;
        if(currentAnimation+1 > players.length-1){
            currentAnimation = 0;
        } else {
            currentAnimation++;
        }
    }

    for(let i = 0; i < players.length; i++){
        players[i].visible = i == currentAnimation;
    }


    //utiliser les touches
    if(nextCoord == yposition && !beginAnimation){
        up = keyboard("ArrowUp"),
        down = keyboard("ArrowDown");
        up.press = () => {
            if(!beginAnimation){
                beginAnimation = upMove();
            }
        };  
        down.press = () => {
            if(!beginAnimation){
                beginAnimation = downMove();
            }
        };
    } else {
        //animation deplacement
        if(nextCoord > yposition){
            yposition += (players[0].vy + delta) * players[0].height/1000;
            if(yposition > nextCoord)yposition = nextCoord;
        } else {
            yposition -= (players[0].vy + delta) * players[0].height/1000;
            if(yposition < nextCoord)yposition = nextCoord;
        }
        players.forEach(player => {
            player.y = yposition;
        })
        if(yposition == nextCoord){
            road += addition;
            beginAnimation = false;
        }
    }

    //score
    scoreMessage.text = "Score : "+points;
    timerPoints += delta;
    if(timerPoints > updatePoints){
        timerPoints = 0;
        points++;
    }

    timerVitesse += delta;
    if(timerVitesse > updateVitesse){
        timerVitesse = 0;
        vitesseDefilement++;
        if(obstacleSpeed > 30)obstacleSpeed-=originalObstacleSpeed/10;
        if(animationSpeed>0.1)animationSpeed-=0.01;
    }
    
  }

  function upMove(){
    if(road == 1)return false;
    nextCoord = yposition - decalage;
    addition = -1;
    return true;
  }

  function downMove(){
    if(road == 3)return false;
    nextCoord = yposition + decalage;
    addition = 1;
    return true;
  }

  function addObstacles(){
      let newObstacle = new Sprite(obstacles[Math.floor(Math.random()*obstacles.length)]);
      newObstacle.zIndex = 10;
      newObstacle.nom = "obstacle";
      decal = Math.floor(Math.random()*3) - 1;
      newObstacle.height = decalage/2;
      newObstacle.width = decalage/2;
      currentObstacles.push(new ObstacleG(newObstacle,decal+2))
      newObstacle.y = positionCentral + decal * decalage;
      app.stage.addChild(newObstacle);
      newObstacle.x = window.innerWidth * 2;
      app.stage.updateLayersOrder();
  }

  function removeObstacle(obstacle) {
      app.stage.removeChild(obstacle.obstacle)
      var index = currentObstacles.indexOf(obstacle);
      if (index > -1) {
        currentObstacles.splice(index, 1);
      }
  }

  

function displayList(){
    app.stage.children.forEach((element)=>{
        console.log(element.nom);
    })
}

function finDuJeu(){
    animationPlayerOn = false;
    vitesseDefilement = 0;
    debut = true
    debutMessage.visible = true;
}

function replay() {
    points = 0;
    animationSpeed = originalAnimationSpeed;
    obstacleSpeed = originalObstacleSpeed;
    vitesseDefilement = vitesseDefilementOriginal;
    animationPlayerOn = true;
    currentObstacles.forEach((obstacle)=>{
        removeObstacle(obstacle)
    })
    yposition = hauteurPage - decalage * 2 + delta;
    players.forEach(player => {
        player.y = yposition;
    })
    road = 2;
}
/* UTILS
    ajouter un sprite : app.stage.addChild(anySprite);
    retirer un sprite : app.stage.removeChild(anySprite)
    changer sa visibilité : anySprite.visible = false;
    changer sa position : sprite.x = valeur, sprite.y = valeur
    changer sa taille : sprite.height = valeur, sprite.width = valeur;
    changer son scale : sprite.scale.x = percentage
    changer sa rotation :  sprite.rotation = 0.5;
    mettre l'ancre au centre : sprite.anchor.x = 0.5; sprite.anchor.y = 0.5; OU sprite.anchor.set(x, y)


    //valeurs
    hauteur : app.stage.height
    largeur : app.stage.width
*/